#!/usr/bin/env python3
import os

from redis import Redis
from flask import Flask, render_template


app = Flask(__name__)


def init_redis():
    host = os.environ["REDIS_HOST"]
    return Redis(host)


@app.route("/")
def home():
    r = init_redis()
    counter = r.incr("flask_app_counter")
    hostname = os.environ["HOSTNAME"]
    return render_template("index.html", hostname=hostname, counter=counter)


if __name__ == "__main__":
    port = int(os.environ.get("PORT", 5000))
    app.run(host="0.0.0.0", port=port)
